import { sendBombSignal } from './nuclear-bomb';

test('请测试 - sendBombSignal 会向 bomb 函数传递 O_o 作为起爆指令', () => {
  const mockCallback = jest.fn();
  sendBombSignal(mockCallback);
  expect(mockCallback).toHaveBeenCalledWith('O_o');
});
